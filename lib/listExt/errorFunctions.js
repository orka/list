/**
 * List extension provides various function to search array with interrups
 * forward/back loop/not spread
 */
define([
    'p!-list/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';

    var errorFunctions = {



        invalidListError: function(__list) {
            if (!this.isValidList(__list) || __list === this) {
                this.emit('error', $error($error.TYPE.INVALID_PARENT, 'Parent cannot be same object as self!', arguments));
                return true;
            }
            return false;
        },

        invalidParentError: function(__item) {
            if (this.invalidListError(__item)) {
                return true;
            }
            if (__item === this) {
                this.emit('error', $error($error.TYPE.INVALID_PARENT, 'Parent cannot be same object as self!', arguments));
                return true;
            }
            return false;
        },


        arrayError: function(__array) {
            if (!$assertful.array(__array)) {
                this.emit('error', $error($error.TYPE.EMPTY_ARRAY, 'Invalid Array', arguments));
                return true;
            }
            return false;
        },

        indexError: function(__index) {
            if (!this.__array__[__index]) {
                this.emit('error', $error($error.TYPE.INVALID_INDEX, 'Invalid index not allowed', arguments));
                return true;
            }
            return false;
        },

        itemError: function(__item) {
            if (!this.isValidItem(__item)) {
                this.emit('error', $error($error.TYPE.INVALID_ITEM, 'Invalid Item', arguments));
                return true;
            }
            return false;
        },

        callbackFunctionError: function(__fn) {
            if (!$assertful.func(__fn)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Callback funciton argument is invalid'));
                return true;
            }
            return false;
        }
    };

    return errorFunctions;
});
