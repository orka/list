/**
 * List extension provides various array like function to manipulate list
 * push|pop|shift|unshift|splice|slice|concat
 */
define([
    'p!-list/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';


    var arrayFunctions = {


        //------------------------     Walking till end or beginning only


        push: function(__item) {
            if (this.itemError(__item)) {
                return this;
            }
            return this.__push__(__item);
        },

        __push__: function(__item) {
            var _index = this.getItemIndex(__item);

            if (_index >= 0) {
                this.__array__.splice(_index, 1);
            }
            this.__array__.push(__item);
            //because no index mapping changhes when new item is added at the end of array
            //just adopt the item
            this.__linkItem__(__item);
            this.emit('length');
            return this;
        },

        unshift: function(__item) {
            if (this.itemError(__item)) {
                return this;
            }
            return this.__unshift__(__item);
        },

        __unshift__: function(__item) {
            var _index = this.getItemIndex(__item);

            if (_index >= 0) {
                this.__array__.splice(_index, 1);
            }

            this.__array__.unshift(__item);
            this.__linkItem__(__item);
            this.emit('length');
            return this;
        },

        shift: function() {
            if (this.__array__.length === 0) {
                return this;
            }
            return this.__shift__();
        },

        __shift__: function() {
            var _item = this.__array__.shift();
            this.__unlinkItem__(_item);

            if (this.__array__.length <= 0) {
                this.__selected__ = null;
                this.__previous__ = null;
                this.emit('length');
                return _item;
            }

            if (_item === this.__selected__) {
                this.__array__[0].select();
            }

            this.emit('length');
            return _item;
        },

        pop: function() {
            if (this.__array__.length === 0) {
                return this;
            }
            return this.__pop__();
        },

        __pop__: function() {
            var _item = this.__array__.pop();
            this.__unlinkItem__(_item);

            if (this.__array__.length <= 0) {
                this.__selected__ = null;
                this.__previous__ = null;
                this.emit('length');
                return _item;
            }

            if (_item === this.__selected__) {
                this.__array__[this.__array__.length - 1].select();
            }

            this.emit('length');
            return _item;
        },

        splice: function(__from, __amount) {
            if (this.indexError(__from) || this.indexError(__amount)) {
                return this;
            }
            return this.__splice__.apply(this, arguments);
        },

        __splice__: function(__from, __amount) {
            var _i, _selectedIndex, _item, _newSelection;
            var _newArray = Array.prototype.slice.call(arguments, 2);
            var _removedArray = this.__array__.splice.apply(this.__array__, arguments);

            //handle removed items - if any
            for (_i = 0; _i < _removedArray.length; _i += 1) {
                _item = _removedArray[_i];
                this.__unlinkItem__(_item);
                //select new item if selected index was removed
                if (this.__selected__ === _item) {
                    _selectedIndex = __from + _i;
                    if (_selectedIndex > this.__array__.length) {
                        _newSelection = this.__array__[this.__array__.length - 1];
                    } else if (_selectedIndex === 0) {
                        _newSelection = this.__array__[0];
                    } else {
                        _newSelection = this.__array__[__from + _newArray.length];
                    }
                }
            }

            //handle new items - if any
            for (_i = 0; _i < _newArray.length; _i += 1) {
                _item = _newArray[_i];
                this.__linkItem__(_item);
            }

            if (_newSelection) {
                _newSelection.select();
            }

            this.emit('length');

            return _removedArray;
        },

        slice: function(__from) {
            return this.__array__.slice(__from);
        },

        concat: function(__array) {
            var _i;

            if (this.arrayError(__array)) {
                return this;
            }

            this.__array__.concat(__array);

            for (_i = 0; _i < __array.length; _i += 1) {
                this.__linkItem__(__array[_i]);
            }

            return this;
        },

        sort: function (__func) {
            if (this.callbackFunctionError(__func)) {
                return null;
            }

            this.__array__.sort(__func);
            return this;
        },

        map: function (__func) {
            if (this.callbackFunctionError(__func)) {
                return null;
            }

            this.__array__.map(__func);
            return this;
        },

        forEach: function (__func) {
            if (this.callbackFunctionError(__func)) {
                return null;
            }

            this.__array__.forEach(__func);
            return this;
        },

        filter: function (__func) {
            if (this.callbackFunctionError(__func)) {
                return null;
            }

            this.__array__.filter(__func);
            this.emit('length');
            return this;
        },

        reverse: function () {
            this.__array__.reverse();
            return this;
        }
    };

    return arrayFunctions;
});
