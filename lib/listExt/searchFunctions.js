/**
 * List extension provides various function to search array with interrups
 * forward/back loop/not spread
 */
define([
    'p!-list/error',
    'p!-assertful',
    'p!-logger'
], function(
    $error,
    $assertful,
    $logger) {
    'use strict';


    var searchFunctions = {


        //------------------------     Walking till end or beginning only


        /**
         * Walks array forward till the end
         * @public
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        searchForward: function(__fn, __fromIndex) {
            //test for errors
            if (this.callbackFunctionError(__fn)) {
                return null;
            }

            if (__fromIndex && this.indexError(__fromIndex)) {
                return null;
            }
            return this.__searchForward__(__fn, __fromIndex);
        },

        /**
         * Walks array forward till the end
         * @private
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        __searchForward__: function(__fn, __fromIndex) {
            var _result;
            var _i = $assertful.int(__fromIndex) ? __fromIndex : this.__selected__.getIndex();
            //try locking array modifications
            this.emit('lock');
            //
            for (; _i < this.__array__.length; _i += 1) {
                _result = __fn(this.__array__[_i]);

                if (!$assertful.noru(_result)) {
                    //try unlocking array modifications
                    this.emit('unlock');
                    return _result;
                }
            }
            //try unlocking array modifications
            this.emit('unlock');
            return null;
        },


        /**
         * Walks array backward till the end
         * @public
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        searchBackward: function(__fn, __fromIndex) {
            //test for errors
            if (this.callbackFunctionError(__fn)) {
                return null;
            }

            if (__fromIndex && this.indexError(__fromIndex)) {
                return null;
            }
            return this.__searchBackward__(__fn, __fromIndex);
        },

        /**
         * Walks array backward till the end
         * @private
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        __searchBackward__: function(__fn, __fromIndex) {
            var _result;
            var _i = $assertful.int(__fromIndex) ? __fromIndex : this.__selected__.getIndex();
            //try locking array modifications
            this.emit('lock');
            //
            for (; _i >= 0; _i -= 1) {
                _result = __fn(this.__array__[_i]);

                if (!$assertful.noru(_result)) {
                    //try unlocking array modifications
                    this.emit('unlock');
                    return _result;
                }
            }
            //try unlocking array modifications
            this.emit('unlock');
            return null;
        },



        //--------------------------------      LOOP search



        /**
         * Walks array forward till the start index looping over the array once
         * @public
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        searchForwardLoop: function(__fn, __fromIndex) {
            //test for errors
            if (this.callbackFunctionError(__fn)) {
                return null;
            }

            if (__fromIndex && this.indexError(__fromIndex)) {
                return null;
            }
            return this.__searchForwardLoop__(__fn, __fromIndex);
        },

        /**
         * Walks array forward till the start index looping over the array once
         * @private
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        __searchForwardLoop__: function(__fn, __fromIndex) {
            var _result;
            var _count = 0;
            var _total = this.__array__.length;
            var _currentIndex = $assertful.int(__fromIndex) ? __fromIndex : this.__selected__.getIndex();
            this.emit('lock');
            while (_count !== _total) {
                //modular will naturally loop over forward
                _currentIndex = (_currentIndex + 1) % _total;

                _result = __fn(this.__array__[_currentIndex]);

                if (!$assertful.noru(_result)) {
                    this.emit('unlock');
                    return _result;
                }

                _count += 1;
            }
            this.emit('unlock');
            return null;
        },

        /**
         * Walks array backward till the start index looping over the array once
         * @public
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        searchBackwardLoop: function(__fn, __fromIndex) {
            //test for errors
            if (this.callbackFunctionError(__fn)) {
                return null;
            }

            if (__fromIndex && this.indexError(__fromIndex)) {
                return null;
            }
            return this.__searchBackwardLoop__(__fn, __fromIndex);
        },

        /**
         * Walks array backward till the start index looping over the array once
         * @private
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        __searchBackwardLoop__: function(__fn, __fromIndex) {
            var _result;
            var _count = 0;
            var _total = this.__array__.length;
            var _currentIndex = $assertful.int(__fromIndex) ? __fromIndex : this.__selected__.getIndex();
            this.emit('lock');
            while (_count !== _total) {
                //make modular to loop over back with positive int
                _currentIndex = (_currentIndex - 1 + _total) % _total;

                _result = __fn(this.__array__[_currentIndex]);

                if (!$assertful.noru(_result)) {
                    this.emit('unlock');
                    return _result;
                }

                _count += 1;
            }
            this.emit('unlock');
            return null;
        },



        //----------------------------------    SPREAD



        /**
         * Walks array backward till the start index looping over the array once
         * @public
         * @param   {Function} __fn      callback function
         * @param   {Integer} __fromIndex start index to search from
         * @returns {*}  result of callback function, if not null | undefined - breaks the loop
         */
        searchSpead: function(__fn, __fromIndex) {
            //test for errors
            if (this.callbackFunctionError(__fn)) {
                return null;
            }

            if (__fromIndex && this.indexError(__fromIndex)) {
                return null;
            }
            return this.__searchSpead__(__fn, __fromIndex);
        },

        /**
         * Seach array from current sletion outward in all directions
         * this method is here for optimizing loops over large arrays
         * @param   {Function} __fn callbacl function if returns NOT undefined - breaks the search
         * @param {Number} __fromIndex Opt, index of native aray to start search from, default - current selectedIndex
         * @returns {[type]}      [description]
         */
        __searchSpead__: function(__fn, __fromIndex) {
            var _result;
            var _count = 0;
            var _total = this.__array__.length;
            var _forward = true;
            var _currentForwardIndex = $assertful.int(__fromIndex) ? __fromIndex : this.__selected__.getIndex();
            var _backwardIndex = _currentForwardIndex;
            var _currentIndex;

            this.emit('lock');
            while (_count !== _total) {
                //set current index
                if (_forward) {
                    _currentIndex = _currentForwardIndex = (_currentForwardIndex + 1) % _total;
                } else {
                    _currentIndex = _backwardIndex = (_backwardIndex - 1 + _total) % _total;
                }

                _result = __fn(this.__array__[_currentIndex]);

                if (!$assertful.noru(_result)) {
                    this.emit('unlock');
                    return _result;
                }

                _count += 1;
                _forward = !_forward;
            }
            this.emit('unlock');
            return null;
        }
    };

    return searchFunctions;
});
