define([
    'p!-list/listExt/searchFunctions',
    'p!-list/listExt/errorFunctions',
    'p!-list/listExt/arrayFunctions'
], function(
    $searchFunctions,
    $errorFunctions,
    $arrayFunctions
) {
    'use strict';

    return {
        searchFunctions: $searchFunctions,
        errorFunctions: $errorFunctions,
        arrayFunctions: $arrayFunctions
    };
});
