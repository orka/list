define(['p!-eventful/emitter', 'p!-list/error'], function($emitter, $error) {
    'use strict';
    var Item = $emitter.subclass('Item', {
        /**
         *
         */
        constructor: function() {
            Item.super.constructor.apply(this);
        },

        deconstructor: function() {
            this.emit('destroyed');
            Item.super.deconstructor.apply(this);
        },

        /**
         * Throws error is object is not an offspring of the Item Class or same as self
         * @param   {Object} __parent Item Class | Subclass instance
         * @returns {Boolean}          true if error had occurred
         */
        invalidItemError: function(__parent) {
            if (!Item.isOffspring(__parent) || this === __parent) {
                this.emit('error', $error($error.TYPE.INVALID_PARENT, 'Unable to set Parent'));
                return true;
            }
            return false;
        },

        invalidParentError: function(__item) {
            if (this.invalidItemError(__item)) {
                return true;
            }
            if (__item === this) {
                this.emit('error', $error($error.TYPE.INVALID_PARENT, 'Parent cannot be same object as self!', arguments));
                return true;
            }
            return false;
        },

        select: function(__options) {
            this.emit('__select__', __options);
            this.emit('select', __options);

            if (this.__parent__ && this.__parent__.getSelected && this.__parent__.getSelected() === this) {
                this.emit('selected', __options);
            } else {
                this.emit('selectFail', __options);
            }
        },

        unselect: function() {
            this.emit('unselect');
        },

        getIndex: function() {
            if (this.__parent__.getItemBy(this.__index) !== this) {
                this.__index = this.__parent__.getIndexOf(this);
            }
            return this.__index;
        },

        /**
         * Parent assignment that emits 'parent' event
         * @param {Object} __list parent list
         * @returns {Object}   self
         */
        setParent: function(__list) {
            if (this.invalidParentError(__list)) {
                return this;
            }

            //emit event in case of override or a new parent
            if (this.__parent__ !== __list) {
                this.__parent__ = __list;
                this.emit('parent', __list);
            }

            return this;
        },

        /**
         * Explicitly quire method to set parents
         * @param   {Object} __list parent list
         */
        __setParent__: function(__list) {
            this.__parent__ = __list;
        },

        /**
         * A simple parent getter
         * @returns {Object|undefined} parent object, instance of rectangle
         */
        getParent: function() {
            return this.__parent__;
        },

        __cleaParent__: function(__list) {
            if (__list === this.__parent__) {
                this.__parent__ = null;
                this.emit('parent', __list);
            }
            return this;
        },

        getNextSibling: function() {
            if (this.__parent__ && this.__parent__.getNextItem) {
                return this.__parent__.getNextFromItem(this.getIndex());
            }
            return null;
        },

        getPrevSibling: function() {
            if (this.__parent__ && this.__parent__.getPrevItem) {
                return this.__parent__.getPrevFromItem(this.getIndex());
            }
            return null;
        }
    });

    return Item;
});
