/**
 * Common error types definitions
 */
define(['p!-error'], function($error) {
    'use strict';

    var type = {
        EMPTY_ARRAY: 'EMPTY_ARRAY',
        INVALID_INDEX: 'INVALID_INDEX',
        INVALID_ITEM: 'INVALID_ITEM',
        INVALID_PARENT: 'INVALID_PARENT'
    };

    var ErrorClass = $error.subclass('ListError', {
        constructor: function() {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
