/**
 * List object is an abstract to navigate an array
 * Array can only be consisted of "Item"s
 */
define([
    'p!-list/item',
    'p!-list/error',
    'p!-list/listExt/main',
    'p!-assertful',
    'p!-logger'
], function(
    $item,
    $error,
    $extentions,
    $assertful,
    $logger) {
    'use strict';

    var List = $item.subclass('List', {

        /**
         * List constructor
         * @param   {Array} __array         Optional argument
         * @param   {Integer} __selectedIndex Options selection index
         */
        constructor: function(__array, __selectedIndex) {
            List.super.constructor.apply(this);

            //prebind function that is called by items on select
            this.__onItemSelect__ = this.__onItemSelect__.bind(this);

            this.setArray(__array || [], Math.abs(__selectedIndex || 0));
        },

        /**
         * List deconstructor
         */
        deconstructor: function() {
            List.super.deconstructor.apply(this);
        },



        //----------------------    Tests



        /**
         * Validates item to be of the lust
         * @param   {Object}  __item Item Class Object
         * @returns {Boolean}        true if itsm is an instance of Item class and is in array
         */
        isValidItem: function(__item) {
            return $item.isOffspring(__item) && !$assertful.noru(this.__array__[__item.getIndex()]);
        },

        isValidList: function(__list) {
            return List.isOffspring(__list);
        },



        //--------------------- Array Managment



        /**
         * Performs checks on argument and sets array
         * Along the way adopting all the items by settng item parents as self
         * @public
         * @param {Array} __array Optional Array, else. empty array is set
         * @param {Number} __selectedIndex optional default selection
         * @returns {Object} self
         */
        setArray: function(__array, __selectedIndex) {
            var _index;

            if (this.arrayError(__array)) {
                return this;
            }

            this.__setArray__(__array);

            _index = $assertful.int(__selectedIndex) ? __selectedIndex % this.__array__.length : 0;

            //a quite selection that should npt produce any event
            this.__setDefaultSelection__((_index + this.__array__.length) % this.__array__.length);
            return this;
        },

        /**
         * Noes not performs checks on argument and sets array
         * Along the way adopting all the items by settng item parents as self
         * @private
         * @param {Array} __array Optional Array, else, empty array is set
         * @returns {Object} self
         */
        __setArray__: function(__array) {
            var _i, _item;

            this.__array__ = __array || [];
            //set parent
            for (_i = 0; _i < this.__array__.length; _i += 1) {
                _item = this.__array__[_i];
                this.__linktItem__(_item);
            }

            this.emit('length');

            return this;
        },

        /**
         * Adopt Item to this list
         * @private
         * @param   {Object} __item Item
         * @returns {Object}        self
         */
        __linktItem__: function(__item) {
            __item.__setParent__(this);
            __item.addListener('__select__', this.__onItemSelect__);
            return this;
        },

        /**
         * Resigns Item from this list
         * @private
         * @param   {Object} __item Item
         * @returns {Object}        self
         */
        __unlinkItem__: function(__item) {
            __item.__clearParent__(this);
            __item.removeListener('__select__', this.__onItemSelect__);
            return this;
        },

        /**
         * Select Event handler
         * Every time Item is selcted - the handler is used
         * Saves selcted and previously selected item attributes
         * @event "selectStart" - before any action is taken
         * @event "select" - main selection even
         * @event "reselect" - when same selection is selcted
         * @event "selectEnd" - after all selection events and logic is complete
         * @param {Object} __item Item caller
         */
        __onItemSelect__: function(__item) {
            this.emit('selectStart');

            this.__previous__ = this.__selected__ || this.__previous__;
            this.__selected__ = __item;

            //previous may be a null|undefined - which is fine
            //for as long as selected is defined
            if (this.__selected__ !== this.__previous__) {
                this.updateDirection();
                if (this.__previous__) {
                    this.__previous__.unselect();
                }

                this.emit('select');
            } else {
                this.emit('reselect');
            }

            this.emit('selectEnd');
        },

        /**
         * A default selection that produces no events or any interactions
         * with selected items
         * @param   {Number} __index int
         */
        __setDefaultSelection__: function(__index) {
            this.__previous__ = undefined;
            this.__selected__ = this.__array__[__index];
        },

        /**
         * sets current direction of movement List.FORWARD | List.BACKWARD
         * @returns {Object} self
         */
        updateDirection: function() {
            if (!this.__previous__ || !this.__selected__) {
                this.__direction = List.FORWARD;
            } else {
                this.__direction = this.__previous__.getIndex() < this.__selected__.getIndex() ? List.FORWARD : List.BACKWARD;
            }
            return this;
        },

        /**
         * Loop ove items and disposes of them
         * @returns {[type]} [description]
         */
        __destroyItems__: function() {
            var _i;
            for (_i = 0; _i < this.__array__.length; _i += 1) {
                this.__array__[_i].deconstructor();
            }
            this.__array__.length = 0;
            return this;
        },



        //------------------------------------------------------------  ARRAY API



        isEmpty: function() {
            return this.__array__.length === 0;
        },

        getLength: function() {
            return this.__array__.length;
        },



        //---------------------------------------------------------  GET INDEX/ITEM



        getIndexOf: function(__item) {
            return this.__array__.indexOf(__item);
        },

        getItemBy: function(__index) {
            return this.__array__[__index];
        },

        getNextIndex: function() {
            return (this.__selected__.getIndex() + 1) % this.__array__.length;
        },

        getPrevIndex: function() {
            return (this.__selected__.getIndex() - 1 + this.__array__.length) % this.__array__.length;
        },

        getNextFromIndex: function(__fromIndex) {
            if (!$assertful.int(__fromIndex)) {
                return null;
            }
            return (__fromIndex + 1) % this.__array__.length;
        },

        getPrevFromIndex: function(__fromIndex) {
            if (!$assertful.int(__fromIndex)) {
                return null;
            }
            return ((__fromIndex % this.__array__.length) - 1 + this.__array__.length) % this.__array__.length;
        },

        getNextItem: function() {
            return this.__array__[this.getNextFromIndex(this.__selected__.getIndex())];
        },

        getPrevItem: function() {
            return this.__array__[this.getPrevFromIndex(this.__selected__.getIndex())];
        },

        getNextFromItem: function(__fromItem) {
            if (this.itemError(__fromItem)) {
                return this.__selected__;
            }
            return this.__array__[this.getNextIndex(__fromItem.getIndex())];
        },

        getPrevFromItem: function(__fromItem) {
            if (this.itemError(__fromItem)) {
                return this.__selected__;
            }
            return this.__array__[this.getPrevIndex(__fromItem.getIndex())];
        },

        getFirst: function() {
            return this.__array__[0];
        },

        getLast: function() {
            return this.__array__[this.__array__.length - 1];
        },

        getSelected: function() {
            return this.__selected__;
        },

        getPreviouslySelected: function() {
            return this.__previous__;
        },

        getSelectedIndex: function() {
            return this.__selected__ ? this.__selected__.getIndex() : undefined;
        },

        getPreviouslySelectedIndex: function() {
            return this.__previous__ ? this.__previous__.getIndex() : undefined;
        },



        //-----------------------------------------------------------------------------     SELECTION



        /**
         * Calls select of rect of next itteration plus offset
         * @param   {Number} __options [description]
         * @returns {Object}          [description]
         */
        selectNext: function(__options) {
            this.getNextItem().select(__options);
            return this;
        },

        /**
         * Calls select of rect of prev itteration minus offset
         * @param   {Number} __options [description]
         * @returns {Object}          [description]
         */
        selectPrev: function(__options) {
            this.getPrevItem().select(__options);
            return this;
        },

        /**
         * Selects first item
         * @returns {Object} self
         */
        selectFirst: function() {
            this.getFirst().select();
            return this;
        },

        /**
         * Selects last item
         * @returns {Object} self
         */
        selectLast: function() {
            this.getLast().select();
            return this;
        },

        /**
         * Finds associated item in array and calls .select()
         * @param   {Integer} __index   array index
         * @param   {*} __options anything that one wants to pass to the select call of item function
         * @returns {Object}           self
         */
        selectIndex: function(__index, __options) {
            if (this.__array__.length === 0) {
                return this;
            }
            if (this.indexError(__index)) {
                return this;
            }
            this.__array__[__index].select(__options);
            return this;
        }
    });

    List.BELLOW_LOOP_POINT = {
        name: 'BELLOW_LOOP_POINT'
    };

    List.ABOVE_LOOP_POINT = {
        name: 'ABOVE_LOOP_POINT'
    };

    List.FORWARD = {
        name: 'FORWARD'
    };

    List.BACKWARD = {
        name: 'BACKWARD'
    };

    List.INVALID_ACTION = {
        name: 'INVALID_ACTION'
    };

    List.mixin($extentions.searchFunctions);
    List.mixin($extentions.errorFunctions);
    List.mixin($extentions.arrayFunctions);

    return List;
});
