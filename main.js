define([
    'p!-list/item',
    'p!-list/list',
    'p!-list/error'
], function(
    $item,
    $list,
    $error) {
    'use strict';
    return {
        list: $list,
        item: $item,
        error: $error
    };
});
