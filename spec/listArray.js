/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['lib/list', 'lib/item'], function($list, $item) {
    'use strict';

    var arrayLength = 100;
    var itemsArray = [];
    var list;

    function makeArray(__length) {
        itemsArray.length = 0;
        while (itemsArray.length < (__length || arrayLength)) {
            itemsArray.push($item());
        }
        return itemsArray;
    }

    function makeList(__length, __index) {
        list = $list(makeArray(__length), __index);
        return list;
    }

    function makeDefaltSelection() {
        makeList(arrayLength);
    }

    describe('When creating list with array of items', function() {
        beforeEach(makeDefaltSelection);

        it('Should have length equal to: ' + arrayLength, function() {
            expect(list.getLength()).toEqual(arrayLength);
        });

        it('Should have default item slected [0]', function() {
            expect(list.getSelected()).toEqual(itemsArray[0]);
        });

        it('Should have previous selection equal to "null"', function() {
            expect(list.getPreviouslySelected()).toEqual(undefined);
        });
    });

    describe('When validating item using .isValidItem()', function() {
        beforeEach(makeDefaltSelection);

        it('Should return "false" when calling if item is not a list item', function() {
            expect(list.isValidItem($item)).toEqual(false);
        });

        it('Should return "true" if item is not a list item', function() {
            expect(list.isValidItem(list.getSelected())).toEqual(true);
        });
    });

    describe('When setting empty array using .setArray()', function() {
        beforeEach(makeDefaltSelection);

        it('Should set new array []', function() {
            expect(list.setArray([]).getLength()).toEqual(0);
        });

        it('Should not have selection if array is empty', function() {
            expect(list.setArray([]).getSelected()).toEqual(undefined);
        });

        it('Should not have previous selection if array is empty', function() {
            expect(list.setArray([]).getPreviouslySelected()).toEqual(undefined);
        });

        it('Should emit "length" event when [] array is set', function() {
            var _called = false;
            list.addListener('length', function() {
                _called = true;
            });
            list.setArray([]);
            expect(_called).toEqual(true);
        });

        it('Should have selected index "undefined"', function() {
            list.setArray([]);
            expect(list.getSelectedIndex()).toEqual(undefined);
        });

        it('Should have previously selected index "undefined"', function() {
            list.setArray([]);
            expect(list.getPreviouslySelectedIndex()).toEqual(undefined);
        });
    });

    describe('When setting non empty array using .setArray()', function() {
        beforeEach(makeDefaltSelection);

        it('Should set new array  ', function() {
            expect(list.setArray(makeArray(50)).getLength()).toEqual(50);
        });

        it('Should have selection [0] if array is not empty', function() {
            var _array = makeArray(50);
            expect(list.setArray(_array).getSelected()).toEqual(_array[0]);
        });

        it('Should not have previous selection if array is not empty', function() {
            var _array = makeArray(50);
            expect(list.setArray(_array).getPreviouslySelected()).toEqual(undefined);
        });

        it('Should emit "length" event when new array is set', function() {
            var _called = false;
            list.addListener('length', function() {
                _called = true;
            });
            list.setArray(makeArray(50));
            expect(_called).toEqual(true);
        });

        it('Should have selected index [0]', function() {
            list.setArray(makeArray(50));
            expect(list.getSelectedIndex()).toEqual(0);
        });

        it('Should have previously selected index "undefined"', function() {
            list.setArray(makeArray(50));
            expect(list.getPreviouslySelectedIndex()).toEqual(undefined);
        });
    });

    describe('When using misc APIs', function() {
        beforeEach(makeDefaltSelection);



        it('Should have .getLast() defiled and return last array item', function() {
            expect(list.getLast()).toEqual(itemsArray[arrayLength - 1]);
        });

        it('Should have .getFirst() defiled and return first array item', function() {
            expect(list.getFirst()).toEqual(itemsArray[0]);
        });

        it('Should have .getNextItem() defiled ', function() {
            expect(list.getNextItem).toBeDefined();
        });

        it('Should have .getNextItem() return [selection + 1] array item', function() {
            expect(list.getNextItem()).toEqual(
                itemsArray[(list.getSelected().getIndex() + 1) % arrayLength]);
        });

        it('Should have .getNextItem() return [0] array item if current selection is last index', function() {
            expect(list.selectLast().getNextItem()).toEqual(
                itemsArray[0]);
        });

        //prev
        it('Should have .getPrevItem() defiled ', function() {
            expect(list.getPrevItem).toBeDefined();
        });

        it('Should have .getPrevItem() return [selection - 1] array item', function() {
            expect(list.getPrevItem()).toEqual(
                itemsArray[(list.getSelected().getIndex() - 1 + arrayLength) % arrayLength]);
        });

        it('Should have .getPrevItem() return [lengs - 10] array item if current selection is last index', function() {
            expect(list.selectFirst().getPrevItem()).toEqual(
                itemsArray[arrayLength - 1]);
        });

        //
        it('Should have .selectNext() select [1] item if [0] is a current selection', function() {
            list.selectIndex(0).selectNext();
            expect(list.getSelected()).toEqual(
                itemsArray[1]);
        });

        it('Should have .selectNext() select [0] item if last was selected', function() {
            list.selectLast().selectNext();
            expect(list.getSelected()).toEqual(
                itemsArray[0]);
        });

        //
        it('Should have .selectPrev() select [length-1] item if [0] is a current selection', function() {
            list.selectFirst().selectPrev();
            expect(list.getSelected()).toEqual(
                itemsArray[arrayLength - 1]);
        });

        it('Should have .selectPrev() select [length - 2] item if last was selected', function() {
            list.selectLast().selectPrev();
            expect(list.getSelected()).toEqual(
                itemsArray[arrayLength - 2]);
        });

    });
});
