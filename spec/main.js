//must specify current path
//this is done to ensure a standalone package dependancies and tests
//are found by requirejs
this.$PROJECT_NAME = 'list';

require.config({
    map: {
        '*': {
            p: 'packages/requirejs/src/package',
            json: 'packages/requirejs/src/json',
            text: 'packages/requirejs/src/text'
        }
    },
    paths: {
        lib: '../lib',
        packages: '../packages'
    }
});

define([
    'item',
    'list',
    'listArray'
], function(
    $item,
    $list) {
    'use strict';
    window.onload();
});
