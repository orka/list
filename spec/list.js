/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['lib/list'], function($list) {
    'use strict';

    describe('When creating list without Array', function() {
        it('Should not throw error', function() {
            expect($list).not.toThrow();
        });

        it('Should not have getParent() defined', function() {
            expect($list().getParent).toBeDefined();
        });

        it('Should not have parent defined', function() {
            expect($list().getParent()).not.toBeDefined();
        });

        it('Should throw if parent is not an list instance', function() {
            var _list = $list();
            expect(_list.setParent.bind(_list)).toThrow();
            expect(_list.setParent.bind(_list, {})).toThrow();
            expect(_list.setParent.bind(_list, [])).toThrow();
            expect(_list.setParent.bind(_list, _list)).toThrow();
            expect(_list.setParent.bind(_list, $list())).not.toThrow();
        });

        it('Should define parent when valid parent set', function() {
            var _list = $list();
            var _parent = $list();
            expect(_list.setParent(_parent).getParent()).toEqual(_parent);
        });

        it('Should emit "parent" event when new parent is set', function() {
            var _list = $list();
            var _parent = $list();
            var _called = false;
            _list.addListener('parent', function() {
                _called = true;
            });
            //set parent
            _list.setParent(_parent);
            expect(_called).toEqual(true);
        });

        it('Should NOT emit "parent" event when same parent is set', function() {
            var _list = $list();
            var _parent = $list();
            var _called = false;
            //call first time
            _list.setParent(_parent);

            _list.addListener('parent', function() {
                _called = true;
            });
            //set parent - second time
            _list.setParent(_parent);
            expect(_called).toEqual(false);
        });

        it('Should not throw when .select() is called', function() {
            var _list = $list();
            expect(_list.select.bind(_list)).not.toThrow();
        });

        it('Should emit "select" event when .select() is called', function() {
            var _list = $list();
            var _called = false;
            _list.addListener('select', function() {
                _called = true;
            });
            //set parent
            _list.select();
            expect(_called).toEqual(true);
        });

        it('Should emit "select" event when .select() is called with parent being set before', function() {
            var _list = $list();
            var _parent = $list();
            var _called = false;
            _list.setParent(_parent);
            _list.addListener('select', function() {
                _called = true;
            });
            //set parent
            _list.select();
            expect(_called).toEqual(true);
        });

        it('Should emit "selectFail" event when .select() is called without parent being set before', function() {
            var _list = $list();
            var _called = false;
            _list.addListener('selectFail', function() {
                _called = true;
            });
            //set parent
            _list.select();
            expect(_called).toEqual(true);
        });

        it('Should not emit "selected" event when .select() is called without parent being set', function() {
            var _list = $list();
            var _called = false;
            _list.addListener('selected', function() {
                _called = true;
            });
            //set parent
            _list.select();
            expect(_called).toEqual(false);
        });

        it('Should return null when .getPrevSibling() is called without parent list being set', function() {
            var _list = $list();
            expect(_list.getPrevSibling()).toEqual(null);
        });

        it('Should return null when .getNextSibling() is called without parent list being set', function() {
            var _list = $list();
            expect(_list.getNextSibling()).toEqual(null);
        });
    });

});
