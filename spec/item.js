/*
global describe,
global it,
global expect,
global beforeEach
global spyOn
 */
define(['lib/item'], function($item) {
    'use strict';

    describe('When creating item', function() {
        it('Should not throw error', function() {
            expect($item).not.toThrow();
        });

        it('Should not have getParent() defined', function() {
            expect($item().getParent).toBeDefined();
        });

        it('Should not have parent defined', function() {
            expect($item().getParent()).not.toBeDefined();
        });

        it('Should throw if parent is not an item instance', function() {
            var _item = $item();
            expect(_item.setParent.bind(_item)).toThrow();
            expect(_item.setParent.bind(_item, {})).toThrow();
            expect(_item.setParent.bind(_item, [])).toThrow();
            expect(_item.setParent.bind(_item, _item)).toThrow();
            expect(_item.setParent.bind(_item, $item())).not.toThrow();
        });

        it('Should define parent when valid parent set', function() {
            var _item = $item();
            var _parent = $item();
            expect(_item.setParent(_parent).getParent()).toEqual(_parent);
        });

        it('Should emit "parent" event when new parent is set', function() {
            var _item = $item();
            var _parent = $item();
            var _called = false;
            _item.addListener('parent', function() {
                _called = true;
            });
            //set parent
            _item.setParent(_parent);
            expect(_called).toEqual(true);
        });

        it('Should NOT emit "parent" event when same parent is set', function() {
            var _item = $item();
            var _parent = $item();
            var _called = false;
            //call first time
            _item.setParent(_parent);

            _item.addListener('parent', function() {
                _called = true;
            });
            //set parent - second time
            _item.setParent(_parent);
            expect(_called).toEqual(false);
        });

        it('Should not throw when .select() is called', function() {
            var _item = $item();
            expect(_item.select.bind(_item)).not.toThrow();
        });

        it('Should emit "select" event when .select() is called', function() {
            var _item = $item();
            var _called = false;
            _item.addListener('select', function() {
                _called = true;
            });
            //set parent
            _item.select();
            expect(_called).toEqual(true);
        });

        it('Should emit "select" event when .select() is called with parent being set before', function() {
            var _item = $item();
            var _parent = $item();
            var _called = false;
            _item.setParent(_parent);
            _item.addListener('select', function() {
                _called = true;
            });
            //set parent
            _item.select();
            expect(_called).toEqual(true);
        });

        it('Should emit "selectFail" event when .select() is called without parent being set before', function() {
            var _item = $item();
            var _called = false;
            _item.addListener('selectFail', function() {
                _called = true;
            });
            //set parent
            _item.select();
            expect(_called).toEqual(true);
        });

        it('Should not emit "selected" event when .select() is called without parent being set', function() {
            var _item = $item();
            var _called = false;
            _item.addListener('selected', function() {
                _called = true;
            });
            //set parent
            _item.select();
            expect(_called).toEqual(false);
        });

        it('Should return null when .getPrevSibling() is called without parent list being set', function() {
            var _item = $item();
            expect(_item.getPrevSibling()).toEqual(null);
        });

        it('Should return null when .getNextSibling() is called without parent list being set', function() {
            var _item = $item();
            expect(_item.getNextSibling()).toEqual(null);
        });
    });
});
